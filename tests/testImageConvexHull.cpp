#include <gtest/gtest.h>
#include <ImageConvexHull.h>
#include <QStringList>
#include <QString>
#include <iostream>
#include <QFileInfo>
#include <math.h>

class ImageConvexHullTest : public testing::Test {
protected:
    // Variables to use        
    QStringList refImages;
    QString intrinsicMatPath;
    QString extrinsicMatPath;
    size_t N;

    virtual void SetUp() {
        setupRefImages();
        setupIntrinsicMatPath();
        setupExtrinsicMatPath();
        N = 2;

        imgCvxHull = new ImageConvexHull(refImages, intrinsicMatPath,
                extrinsicMatPath,
                N);
    }

    virtual void TearDown() {
        delete imgCvxHull;
    }

    void setupRefImages() {
        QFileInfo img1("resource/000000.png");
        QFileInfo img2("resource/000050.png");
        QFileInfo img3("resource/000100.png");
        QFileInfo img4("resource/000150.png");
        QFileInfo img5("resource/000200.png");
        QFileInfo img6("resource/000250.png");
        QFileInfo img7("resource/000300.png");
        QFileInfo img8("resource/000350.png");
        QFileInfo img9("resource/9999999.png");
        QFileInfo img10("resource/000010.png");
        QFileInfo img11("resource/000011.png");
        QFileInfo img12("resource/jpg_0000.jpg");
        refImages.push_back(img1.absoluteFilePath());
        refImages.push_back(img2.absoluteFilePath());
        refImages.push_back(img3.absoluteFilePath());
        refImages.push_back(img4.absoluteFilePath());
        refImages.push_back(img5.absoluteFilePath());
        refImages.push_back(img6.absoluteFilePath());
        refImages.push_back(img7.absoluteFilePath());
        refImages.push_back(img8.absoluteFilePath());
        refImages.push_back(img9.absoluteFilePath());
        refImages.push_back(img10.absoluteFilePath());
        refImages.push_back(img11.absoluteFilePath());
        refImages.push_back(img12.absoluteFilePath());
    }

    void setupIntrinsicMatPath() {
        QFileInfo intrinsicMat("resource/intrinsics.txt");
        intrinsicMatPath = intrinsicMat.absoluteFilePath();
    }

    void setupExtrinsicMatPath() {
        QFileInfo extrinsicMat("resource/extrinsics.txt");
        extrinsicMatPath = extrinsicMat.absoluteFilePath();
    }

    ImageConvexHull* imgCvxHull;

    bool isApproximate(double expected, double actual) {
        double delta = 0.001;
        if (fabs(expected - actual) < delta) {
            return true;
        } else {
            return false;
        }
    }
};

TEST_F(ImageConvexHullTest, setIntrinsicMatTest) {
    std::pair<bool, std::string> res = imgCvxHull->setIntrinsicMatrices(QString("resource/intrinsics.txt"));    
    EXPECT_TRUE(res.first);    
}

TEST_F(ImageConvexHullTest, setExtrinsicMatTest){
    std::pair<bool, std::string> res = imgCvxHull->setExtrinsicMatricesAndCamIndices(QString("resource/extrinsics.txt"));
    EXPECT_TRUE(res.first);
}

TEST_F(ImageConvexHullTest, ConvexHullArea_SimpleTest){
    double data[2][4] = {{0, 1, 0, 1},
                        {0, 0, 1, 1}};
    cv::Mat pts = cv::Mat(2,4, CV_64F, data);
    cv::Mat contour = imgCvxHull->getContour(pts);
    double convexHullArea = cv::contourArea(contour);
    EXPECT_EQ(convexHullArea, 1);
}

TEST_F(ImageConvexHullTest, parseImgPathTest){
    QString imgName = "/home/candrastefan/11/000000.jpg";
    QString imgName2 = "/home/candrastefan/11/000100.jpg";
    QString imgName3 = "000101.png";
    QString imgName4 = "10009.bmp";
    
    EXPECT_EQ(QString("000000.jpg"), ImageConvexHull::parseImgPath(imgName));
    EXPECT_EQ(QString("000100.jpg"), ImageConvexHull::parseImgPath(imgName2));
    EXPECT_EQ(QString("000101.png"), ImageConvexHull::parseImgPath(imgName3));
    EXPECT_EQ(QString("10009.bmp"), ImageConvexHull::parseImgPath(imgName4));
}

TEST_F(ImageConvexHullTest, filterByColTest){
    // Test that entries in input and output are the same if bool is all true
    double data[2][9] = {{621.6006305413606, 627.2528612733714, 625.3954083243898, 622.5957075912535, 619.6328083586591, 625.2882865015039, 623.4355268440617, 621.6987252480139, 625.5009391199412},
                         {233.9340590269903, 233.9747135766625, 233.9480926278771, 240.4183432819987, 269.845604572018, 269.8196126671016, 269.8218583513735, 320.8666347977309, 320.9102879277656}};
    cv::Mat uv = cv::Mat(2, 9, CV_64F, &data);
    bool isInbound[1][9] = {{true, true, true, true, true, true, true, true, true}};
    cv::Mat b = cv::Mat(1, 9, CV_8U, &isInbound);
    
    cv::Mat inbound = imgCvxHull->filterByCol(uv, b);
    EXPECT_EQ(9, inbound.cols);
    for(int r = 0; r < uv.rows; r++){
        for (int c = 0; c < uv.cols; c++){
            EXPECT_EQ(uv.at<double>(r,c), inbound.at<double>(r,c));
        }
    }
}

TEST_F(ImageConvexHullTest, getContourTest){
    // Simple test
    double data[2][9] = {{621.6006305413606, 627.2528612733714, 625.3954083243898, 622.5957075912535, 619.6328083586591, 625.2882865015039, 623.4355268440617, 621.6987252480139, 625.5009391199412},
                         {233.9340590269903, 233.9747135766625, 233.9480926278771, 240.4183432819987, 269.845604572018, 269.8196126671016, 269.8218583513735, 320.8666347977309, 320.9102879277656}};
    cv::Mat uv = cv::Mat(2, 9, CV_64F, &data);
    cv::Mat contour = imgCvxHull->getContour(uv);
    EXPECT_FALSE(contour.empty());
    double area = cv::contourArea(contour);
    EXPECT_TRUE(area > 0);
}

TEST_F(ImageConvexHullTest, GetBestImages){
    std::vector<CCVector3> boundedPts;
    EXPECT_TRUE(imgCvxHull->getBestImages(boundedPts).empty());    
}

TEST_F(ImageConvexHullTest, GetBestImages_InboundPts){
    std::vector<CCVector3> pts;
    CCVector3 a(15.7, 8.874, 0.596);
    pts.push_back(a);
    CCVector3 b(15.6, 8.875, 0.5976);
    pts.push_back(b);
    CCVector3 c(15.49, 9.026, 0.5966);
    pts.push_back(c);    
        
    imgCvxHull->setN(3);
            
    // Best images should not be empty        
    std::vector<ImageConvexHull::ConvexHullProp> bestImgs = imgCvxHull->getBestImages(pts);
    ASSERT_FALSE(bestImgs.empty());
    EXPECT_EQ(1, bestImgs.size());
}

//
//TEST_F(ImageConvexHullTest, coordTrans3DTest1){
//    double data[3][5] = {{12.84099960327148, 12.82400035858154, 12.79800033569336, 12.77999973297119, 12.75399971008301},
//                        {3.048199892044067, 3.084199905395508, 3.029900074005127, 3.064899921417236, 3.098900079727173},
//                        {0.1585599929094315, 0.1580599993467331, 0.07490699738264084, 0.07442399859428406, 0.07497300207614899}};
//    cv::Mat pts = cv::Mat(3,5, CV_64F, &data);
////    imgCvxHull->setMAT(QString("resource/0000.mat"), 2);
////    size_t imgIndex = 12;
////    cv::Mat imut0_to_imut = imgCvxHull->testGetImutToImut0(imgIndex).inv();
//    
//    double inv[4][4] = {{0.9885, 0.1510, -0.0119, -3.6251},
//                        {-0.1510, 0.9885, -0.0020, 0.4561},
//                        {0.0115, 0.0038, 0.9999, -0.1627},
//                        {0,0,0,1}};
//    cv::Mat correct_imut0_to_imut(4,4, CV_64F, &inv);
////    for (int r = 0; r < correct_imut0_to_imut.rows; r++){
////        for (int c = 0; c < correct_imut0_to_imut.cols; c++){
////            EXPECT_TRUE(isApproximate(correct_imut0_to_imut.at<double>(r,c), imut0_to_imut.at<double>(r,c))) << "r = " << r << ", c = " << c << "; expected = " << correct_imut0_to_imut.at<double>(r,c) << ", actual = " << imut0_to_imut.at<double>(r,c);
//////            EXPECT_EQ(correct_imut0_to_imut.at<double>(r,c), imut0_to_imut.at<double>(r,c))  << "r = " << r << ", c = " << c;
////        }
////    }
//    
//    cv::Mat ptsImut = imgCvxHull->coordTrans3D(correct_imut0_to_imut, pts);
//    
//    double cor[3][5] = {{9.5261, 9.5147, 9.4818, 9.4693, 9.4487},
//                            {1.5299, 1.5681, 1.5185, 1.5558, 1.5934},
//                            {0.1547, 0.1541, 0.0705, 0.0699, 0.0703}};
//    cv::Mat correct_imut(3,5, CV_64F, &cor);
//    for (int r = 0; r < correct_imut.rows; r++){
//        for (int c = 0; c < correct_imut.cols; c++){
//            EXPECT_TRUE(isApproximate(correct_imut.at<double>(r,c), ptsImut.at<double>(r,c))) << "r = " << r << ", c = " << c << "; expected = " << correct_imut.at<double>(r,c) << ", actual = " << ptsImut.at<double>(r,c);
////            EXPECT_EQ(correct.at<double>(r,c), ptsImut.at<double>(r,c));
//        }
//    }
//    
////    cv::Mat ptsCamt = imgCvxHull->testCoordTrans3D(imgCvxHull->testGetImuToCam(), ptsImut);
////    double cor_camt[3][5] = {{-1.7775, -1.8158, -1.7677, -1.8051, -1.8428},
////                                {0.6571, 0.6570, 0.7409, 0.7408, 0.7396},
////                                {8.4502, 8.4391, 8.4047, 8.3925, 8.3723}};
////    cv::Mat correct_camt(3,5, CV_64F, &cor_camt);
////    for (int r = 0; r < correct_camt.rows; r++){
////        for (int c = 0; c < correct_camt.cols; c++){
////            EXPECT_TRUE(isApproximate(correct_camt.at<double>(r,c), ptsCamt.at<double>(r,c))) << "r = " << r << ", c = " << c << "; expected = " << correct_camt.at<double>(r,c) << ", actual = " << ptsCamt.at<double>(r,c);
//////            EXPECT_EQ(correct.at<double>(r,c), ptsImut.at<double>(r,c));
////        }
////    }
//}
//
////TEST_F(ImageConvexHullTest, getIntrinsicMatTest){
////    imgCvxHull->setMAT(QString("resource/0000.mat"), 2);
////    cv::Mat intrinsic = imgCvxHull->testGetIntrinsicMat();
////        
////    double cor[3][4] = {{721.5377, 0, 609.5593, 44.8573},
////                            {0, 721.5377, 172.8540, 0.2164},
////                            {0, 0, 1, 0.0027}};    
////    cv::Mat correct_intrinsic(3,4, CV_64F, &cor);
////    ASSERT_EQ(correct_intrinsic.rows, intrinsic.rows);
////    ASSERT_EQ(correct_intrinsic.cols, intrinsic.cols);
////    
////    for (int r = 0; r < correct_intrinsic.rows; r++){
////        for (int c = 0; c < correct_intrinsic.cols; c++){
////            EXPECT_TRUE(isApproximate(correct_intrinsic.at<double>(r,c), intrinsic.at<double>(r,c))) << "r = " << r << ", c = " << c << "; expected = " << correct_intrinsic.at<double>(r,c) << ", actual = " << intrinsic.at<double>(r,c);
//////            EXPECT_EQ(correct.at<double>(r,c), ptsImut.at<double>(r,c));
////        }
////    }
////}
//
//TEST_F(ImageConvexHullTest, getIntrinsicMatTest2){
//    imgCvxHull->setIntrinsicMatrices(QString("resource/intrinsic.txt"));
//    size_t camIndex = 2;
//    cv::Mat intrinsic = imgCvxHull->getIntrinsicMat(camIndex);
//        
//    double cor[3][4] = {{721.5377, 0, 609.5593, 0},
//                            {0, 721.5377, 172.8540, 0},
//                            {0, 0, 1, 0}};    
//    
//    cv::Mat correct_intrinsic(3,4, CV_64F, &cor);
//    ASSERT_EQ(correct_intrinsic.rows, intrinsic.rows);
//    ASSERT_EQ(correct_intrinsic.cols, intrinsic.cols);
//    
//    for (int r = 0; r < correct_intrinsic.rows; r++){
//        for (int c = 0; c < correct_intrinsic.cols; c++){
//            EXPECT_TRUE(isApproximate(correct_intrinsic.at<double>(r,c), intrinsic.at<double>(r,c))) << "r = " << r << ", c = " << c << "; expected = " << correct_intrinsic.at<double>(r,c) << ", actual = " << intrinsic.at<double>(r,c);
////            EXPECT_EQ(correct.at<double>(r,c), ptsImut.at<double>(r,c));
//        }
//    }
//}
//
//TEST_F(ImageConvexHullTest, proj3Dto2DTest){
//    double pts_camt[3][5] = {{-1.7775, -1.8158, -1.7677, -1.8051, -1.8428},
//                                {0.6571, 0.6570, 0.7409, 0.7408, 0.7396},
//                                {8.4502, 8.4391, 8.4047, 8.3925, 8.3723}};
//    cv::Mat ptsCamt(3,5, CV_64F, &pts_camt);
//    cv::Mat intrinsicMat = imgCvxHull->getIntrinsicMat();
//    ImageConvexHull::ProjProp3DTo2D result = imgCvxHull->project3DToCam2D(intrinsicMat, ptsCamt, 0, 0);
//    cv::Mat actual = result.uv;
//    double expected[2][5] = {{462.9418038174206, 459.475459072477, 462.9893775263937, 459.5620016720407, 455.9522403483468},
//                            {228.9130557706053, 228.9782194512283, 236.4082626582863, 236.4920253311309, 236.5421316681589}};
//    cv::Mat correct(2,5, CV_64F, &expected);
//    for (int r = 0; r < correct.rows; r++){
//        for (int c = 0; c < correct.cols; c++){
//            EXPECT_TRUE(isApproximate(correct.at<double>(r,c), actual.at<double>(r,c))) << "r = " << r << ", c = " << c << "; expected = " << correct.at<double>(r,c) << ", actual = " << actual.at<double>(r,c);
////            EXPECT_EQ(correct.at<double>(r,c), ptsImut.at<double>(r,c));
//        }
//    }    
//}
//
////TEST_F(ImageConvexHullTest, transAndProj3DToCam2DTest){
////    double data[3][5] = {{12.84099960327148, 12.82400035858154, 12.79800033569336, 12.77999973297119, 12.75399971008301},
////                        {3.048199892044067, 3.084199905395508, 3.029900074005127, 3.064899921417236, 3.098900079727173},
////                        {0.1585599929094315, 0.1580599993467331, 0.07490699738264084, 0.07442399859428406, 0.07497300207614899}};
////    cv::Mat boundedPts = cv::Mat(3,5, CV_64F, &data);
////    imgCvxHull->setMAT(QString("resource/0000.mat"), 2);        
////    size_t matIndex = 0;
////    ImageConvexHull::ProjProp3DTo2D projection = imgCvxHull->transAndProj3Dto2D(boundedPts, matIndex);
////    cv::Mat uv = projection.uv;
////    
////    double correct[2][5] = {{413.0664, 410.5706, 413.3562, 410.8953, 408.3480},
////                            {214.1261, 214.1709, 219.4057, 219.4613, 219.4798}};
////    cv::Mat correctProjPts = cv::Mat(2, 5, CV_64F, &correct);
////    ASSERT_EQ(correctProjPts.rows, uv.rows);
////    ASSERT_EQ(correctProjPts.cols, uv.cols);
////    EXPECT_EQ(correctProjPts.type(), uv.type());   
////    
////    for (int r = 0; r < correctProjPts.rows; r++){
////        for (int c = 0; c < correctProjPts.cols; c++){
////            EXPECT_TRUE(isApproximate(correctProjPts.at<double>(r,c), uv.at<double>(r,c)))  << "r = " << r << ", c = " << c << "; expected = " << correctProjPts.at<double>(r,c) << ", actual = " << uv.at<double>(r,c);
//////            EXPECT_EQ(correctProjPts.at<double>(r,c), uv.at<double>(r,c));
////        }
////    }
////    
////    // Test for isFront and isInbound
////    double pts[3][12] = {{46.40999984741211, 46.40599822998047, 46.35200119018555, 46.30099868774414, 46.25400161743164, 46.09199905395508, 46.09600067138672, 46.13199996948242, 45.80599975585938, 45.67200088500977, 45.67100143432617, 45.70100021362305},
////                        {11.85799980163574, 11.90499973297119, 11.98900032043457, 12.07400035858154, 12.15999984741211, 12.2180004119873, 11.51200008392334, 11.61400032043457, 11.85000038146973, 12.03999996185303, 12.1230001449585, 12.21000003814697},
////                        {-1.019400000572205, -1.02020001411438, -1.019000053405762, -1.018800020217896, -1.017699956893921, -1.009600043296814, -1.177700042724609, -1.183099985122681, -1.170899987220764, -1.167899966239929, -1.171300053596497, -1.176100015640259}};
////    cv::Mat bPts(3, 12, CV_64F, &pts);
////    size_t imgIndex = 20;
////    projection = imgCvxHull->transAndProj3Dto2D(bPts, imgIndex, 375, 1242);
////    std::cout << "projection.uv = \n" << projection.uv << std::endl;
////    std::cout << "projection.isFront = \n" << projection.isFront << std::endl;
////    std::cout << "projection.isInbound = \n" << projection.isInbound << std::endl;
////    std::cout << "projection.depth = \n" << projection.depth << std::endl;
////}
//
