/* 
 * File:   ImageConvexHull.cpp
 * Author: Stefanus Candra
 * 
 * Class to:
 * 1. project 3D point cloud to 2D images
 * 2. draw convex hull on 2D images
 * 3. calculate size of convex hull on 2D images 
 * 
 * Created on June 21, 2014, 5:42 PM
 */

// Standard library
#include <vector>
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>

// Qt
#include <QFileInfo>

#include <ImageConvexHull.h>

// Etc
#include <glog/logging.h>
#include <typeinfo>
#include <boost/algorithm/string.hpp>

std::vector<std::string> split(const std::string &s, char delim);

ImageConvexHull::ImageConvexHull() {
};

ImageConvexHull::ImageConvexHull(const QStringList& imagesPaths,
        const QString& intrinsicMatPath,
        const QString& extrinsicMatPath,
        const size_t& N) {
    init(imagesPaths, intrinsicMatPath, extrinsicMatPath, N);
}

ImageConvexHull::~ImageConvexHull() {
    for (auto iterator = m_intrinsicMats.begin(); iterator != m_intrinsicMats.end(); iterator++){
        iterator->second.release();
    }
    
    for (auto iterator = m_extrinsicMats.begin(); iterator != m_extrinsicMats.end(); iterator++){
        iterator->second.release();
    }
    google::ShutdownGoogleLogging();
}

void ImageConvexHull::init(const QStringList& imagesPaths,
        const QString& intrinsicMatPath,
        const QString& extrinsicMatPath,
        const size_t N) {

    setN(N);
    setRefImagesPaths(imagesPaths);
    setIntrinsicMatrices(intrinsicMatPath);
    setExtrinsicMatricesAndCamIndices(extrinsicMatPath);

    // Enable logging
    m_className = typeid (this).name();
    initLogging();
}

void ImageConvexHull::initLogging() {
    google::SetLogDestination(google::INFO, "INFO.log");
    google::SetLogDestination(google::WARNING, "WARNING.log");
    google::SetLogDestination(google::ERROR, "ERROR.log");
    google::SetLogDestination(google::FATAL, "FATAL.log");
    google::InitGoogleLogging(m_className);
    LOG(INFO) << m_className << " initialized";
}

/*
 * The most important method for this class which is to get the best reference images
 * Input: an empty QStringList
 * Return: a list of image filenames with the biggest convex hull area
 */
std::vector< ImageConvexHull::ConvexHullProp > ImageConvexHull::getBestImages(const std::vector<CCVector3>& boundedPts) {
    std::vector< ImageConvexHull::ConvexHullProp > convexHullProps;
    if (boundedPts.empty()) {
        return convexHullProps;
    }

    cv::Mat pts3D = convertPts3D(boundedPts);
//    LOG(INFO) << "Bounded points = \n" << pts3D;

    // Initialize vector
    std::vector< std::pair<double, QString> > pairs;

    // Loop through all the images
    for (int i = 0; i < m_refImagesPaths.size(); ++i) {
        QString currImg = m_refImagesPaths.at(i);
        std::string currentImgStr = currImg.toUtf8().constData();
        LOG(INFO) << "Processing image: " << currentImgStr;

        // Get image size without loading the image                
        boost::gil::point2<long> imageSize = getImageSize(currentImgStr);        

        // Parse the image filename to give corresponding index        
        QString fileName = parseImgPath(currImg);

        ProjProp3DTo2D projection = transAndProj3Dto2D(pts3D, fileName, imageSize.y, imageSize.x);
        if (projection.uv.empty()) {
            continue;
        }

//        LOG(INFO) << "Projection.uv = \n" << projection.uv;
//        LOG(INFO) << "Projection.isFront = \n" << projection.isFront;
//        LOG(INFO) << "Projection.isInbound = \n" << projection.isInbound;

        // Calculate area of convex hull of points that are inbounds
        cv::Mat inboundPts = filterByCol(projection.uv, projection.isInbound);
//        LOG(INFO) << "inboundPts = \n" << inboundPts;
        cv::Mat contour = getContour(inboundPts);
//        LOG(INFO) << "contour = \n" << contour;
        if (!contour.empty()) {
            double convexHullArea = cv::contourArea(contour);
            LOG(INFO) << "convexHullArea = " << convexHullArea;

            // Store the pair if area is greater than 0
            if (convexHullArea > 0) {
                ImageConvexHull::ConvexHullProp chProp = ImageConvexHull::ConvexHullProp();
                chProp.convexHullArea = convexHullArea;
                chProp.imgPath = m_refImagesPaths.at(i);
                chProp.vertices2D = contour;
                convexHullProps.push_back(chProp);
            }
        }
        
        // Release the matrices
        contour.release();
        inboundPts.release();
    }

    if (!convexHullProps.empty()) {
        // Turn vector into max heap
        std::make_heap(convexHullProps.begin(), convexHullProps.end());
        // Remove the bottom elements, leaving the top m_N elements
        int numToRemove = convexHullProps.size() - m_N;
        for (int i = 0; i < numToRemove; i++) {
            convexHullProps.pop_back();
        }
    }
    
    // Release the matrix
    pts3D.release();

    return convexHullProps;
}

ImageConvexHull::ProjProp3DTo2D ImageConvexHull::transAndProj3Dto2D(const std::vector<CCVector3>& pts3D, const QString& imgName, size_t height, size_t width) {
    return transAndProj3Dto2D(convertPts3D(pts3D), imgName, height, width);
}

/* 
 * Transform from 3D Global(T=0) IMU coordinate to 2D local camera coordinate
 */
ImageConvexHull::ProjProp3DTo2D ImageConvexHull::transAndProj3Dto2D(const cv::Mat& pts3D, const QString& imgName, size_t height, size_t width) {
    ProjProp3DTo2D ret;

    // Get the corresponding imut_to_imut0 in OpenCV matrix format
    cv::Mat extrinsicMat = getExtrinsicMat(imgName.toUtf8().constData());
    
    const size_t camIndex = getCamIndex(imgName.toUtf8().constData());
    if (extrinsicMat.empty()) {
        LOG(WARNING) << "Cannot get extrinsic matrix for imgName = " << imgName.toUtf8().constData();
        return ret;
    }

    // Transform to camera local coordinate system    
    cv::Mat ptsCamt = coordTrans3D(extrinsicMat, pts3D);    
    cv::Mat intrinsicMat = getIntrinsicMat(camIndex);
    if (intrinsicMat.empty()) {
        LOG(WARNING) << "Cannot get intrinsic matrix for camIndex = " << camIndex;
        return ret;
    }
    ret = project3DToCam2D(intrinsicMat, ptsCamt, height, width);    
    // Release the matrices
    extrinsicMat.release();
    ptsCamt.release();
    intrinsicMat.release();
    
    return ret;
}

cv::Mat ImageConvexHull::getContour(const cv::Mat& pts) {
    cv::Mat contour; // Convex hull contour points

    cv::Mat pts2D = pts.clone();

    if (pts2D.empty()) {
        return contour;
    }
    // Convert to CV_32F because it's required by convexHull
    pts2D.convertTo(pts2D, CV_32F);
    // Convert to 2 channels because it's required
    std::vector<cv::Mat> channels;
    channels.push_back(pts2D.row(0));
    channels.push_back(pts2D.row(1));
    cv::merge(channels, pts2D);

    cv::Mat hull; // Convex hull points

    double epsilon = 0.001; // Contour approximation accuracy

    cv::convexHull(pts2D, hull);

    // Approximating polygonal curve to convex hull
    cv::approxPolyDP(hull, contour, epsilon, true);
    
    // Release matrices
    pts2D.release();
    hull.release();
    for (int i = 0; i < channels.size(); i++){
        channels.at(i).release();
    }

    return contour;
}

/*
 * Input: 2 matrices A and B:
 *      A is a 2D matrix
 *      B is a boolean matrix
 * Return: A matrix of elements of A where the corresponding index on B is true
 */
cv::Mat ImageConvexHull::filterByCol(const cv::Mat& A, const cv::Mat& bools) {

    CHECK_EQ(A.cols, bools.cols);
    CHECK_EQ(1, bools.rows);

    int nonZeroCount = cv::countNonZero(bools);
    int count = 0;
    cv::Mat ret(A.rows, nonZeroCount, A.type());
    for (int c = 0; c < bools.cols; c++) {
        if (bools.at<bool>(0, c) != 0) {
            A.col(c).copyTo(ret.col(count));
            count++;
        }
    }

    CHECK_EQ(nonZeroCount, count);

    return ret;
}

/* Return: 0 if successful, nonzero otherwise
 * TODO: fill the angle and depth of the ret struct
 */
ImageConvexHull::ProjProp3DTo2D ImageConvexHull::project3DToCam2D(cv::Mat& intrinsicMat,
        cv::Mat& ptsCamt,
        const size_t imgHeight,
        const size_t imgWidth,
        cv::Mat T) {
    CHECK_EQ(intrinsicMat.rows, 3);

    // Initialize the struct to return
    ProjProp3DTo2D ret = ImageConvexHull::ProjProp3DTo2D();

    if (intrinsicMat.cols != 3) {
        if (intrinsicMat.cols == 4) {
            cv::Mat ones = cv::Mat::ones(1, ptsCamt.cols, ptsCamt.type());
            ptsCamt.push_back(ones);
            ones.release();
        } else {
            return ret;
        }
    }
    cv::Mat temp = intrinsicMat * ptsCamt;
    // Get the x and y coordinate i.e. row 0 and row 1
    cv::Mat uv(2, ptsCamt.cols, ptsCamt.type());
    temp.row(0).copyTo(uv.row(0));
    temp.row(1).copyTo(uv.row(1));

    cv::Mat zeroes = cv::Mat::zeros(1, ptsCamt.cols, ptsCamt.type());
    cv::Mat front = temp.row(2) > zeroes;
    ret.isFront = front;
    zeroes.release();
    front.release();
    
    // Element-wise division
    cv::Mat divisor = cv::repeat(temp.row(2), 2, 1);
    CHECK_EQ(divisor.rows, 2);
    CHECK_EQ(divisor.rows, uv.rows);
    CHECK_EQ(divisor.cols, uv.cols);
    uv = uv / divisor;
    divisor.release();

    ret.uv = uv;
    ret.depth = ptsCamt.row(2);

    // TODO: fill the angle and depth of the ret struct
    cv::Mat isInbound(1, uv.cols, CV_8U);
    isInbound = ret.isFront &
            (uv.row(0) > 0.5) &
            (uv.row(0) < (imgWidth + 0.5)) &
            (uv.row(1) > 0.5) &
            (uv.row(1) < (imgHeight + 0.5));
    ret.isInbound = isInbound;    
    CHECK_EQ(ret.isInbound.cols, ret.isFront.cols);
    CHECK_EQ(ret.isInbound.cols, uv.cols);
    
    isInbound.release();
    temp.release();
        
    return ret;
}

cv::Mat ImageConvexHull::getIntrinsicMat(const size_t camIndex) {
    return m_intrinsicMats[camIndex];
}

cv::Mat ImageConvexHull::getExtrinsicMat(const std::string& imgName) {
    return m_extrinsicMats[imgName];
}

const size_t ImageConvexHull::getCamIndex(const std::string& imgName) {
    return m_camIndices[imgName];
}

/*
 * Transform 3D points from a coordinate system to another, given a transformation matrix
 */
cv::Mat ImageConvexHull::coordTrans3D(const cv::Mat& transMat, const cv::Mat& pts3D) {
    CHECK_EQ(4, transMat.cols);
    CHECK_EQ(4, transMat.rows);

    cv::Mat clone = pts3D.clone();
    cv::Mat ones = cv::Mat::ones(1, pts3D.cols, pts3D.type());
    clone.push_back(ones);

    CHECK_EQ(transMat.cols, clone.rows);
    // Transform
    clone = transMat * clone;

    // Remove the 4th row
    clone.pop_back();
    CHECK_EQ(3, clone.rows);
    
    ones.release();

    return clone;
}

std::pair<bool, std::string> ImageConvexHull::setIntrinsicMatrices(const QString& path) {
    m_intrinsicMats.clear();
    
    QByteArray ba = path.toUtf8();
    std::ifstream input(ba.constData());
    size_t numRow = 3;
    size_t numCol = 4;
    std::string line;

    if (input.is_open()) {
        while (std::getline(input, line)) {
            std::vector<std::string> l = split(line, ' ');
            if (l.empty()) {
                continue;
            } else if (l.size() != (numRow * numCol + 1)) {                
                return std::make_pair(false, "Intrinsic matrix: number of elements in one of the line is not correct");
            } else {
                std::string camIndexStr = l.at(0);
                std::stringstream ss(camIndexStr);
                size_t camIndex;
                ss >> camIndex;

                cv::Mat intrinsicMat(numRow, numCol, CV_64F);

                for (int c = 0; c < numCol; c++) {
                    for (int r = 0; r < numRow; r++) {
                        intrinsicMat.at<double>(r, c) = std::stod(l.at(1 + c * numRow + r));
                    }
                }

                if (m_intrinsicMats.find(camIndex) != m_intrinsicMats.end()) {
                    intrinsicMat.release();
                    m_intrinsicMats.clear();
                    return std::make_pair(false, "More than one intrinsic matrix for the same camera index");
                } else {
                    // Insert entry to intrinsic matrix
                    m_intrinsicMats[camIndex] = intrinsicMat;
                    intrinsicMat.release();
                }
            }
        }
        
        input.close();
    } else {
        LOG(WARNING) << "Unable to open file" << ba.constData();
        return std::make_pair(false, "Unable to open file");
    }
    
    return std::make_pair(true, "Valid intrinsic matrices");
}

std::pair<bool, std::string> ImageConvexHull::setExtrinsicMatricesAndCamIndices(const QString& path){
    m_extrinsicMats.clear();
    m_camIndices.clear();
    
    QByteArray ba = path.toUtf8();
    std::ifstream input(ba.constData());
    size_t numRow = 4;
    size_t numCol = 4;
    std::string line;

    if (input.is_open()) {
        while (std::getline(input, line)) {
            std::vector<std::string> l = split(line, ' ');
            if (l.empty()) {
                continue;
            } else if (l.size() != (numRow*numCol + 2)) {                
                return std::make_pair(false, "Extrinsic matrix: number of elements in one of the line is not correct");
            } else {
                std::string imgName = l.at(0);
                std::string camIndexStr = l.at(1);
                std::stringstream ss(camIndexStr);
                size_t camIndex;
                ss >> camIndex;

                cv::Mat extrinsicMat(numRow, numCol, CV_64F);
                // Parsing the numbers into OpenCV Mat
                for (int c = 0; c < numCol; c++) {
                    for (int r = 0; r < numRow; r++) {
                        extrinsicMat.at<double>(r, c) = std::stod(l.at(2 + c * numRow + r));
                    }
                }

                if (m_extrinsicMats.find(imgName) != m_extrinsicMats.end()) {
                    extrinsicMat.release();
                    m_extrinsicMats.clear();
                    return std::make_pair(false, "More than one extrinsic matrix for the same image name");
                } else {
                    // Insert entry to extrinsic matrix
                    m_extrinsicMats[imgName] = extrinsicMat;
                    extrinsicMat.release();
                }
                
                if (m_camIndices.find(imgName) != m_camIndices.end()) {                    
                    m_camIndices.clear();
                    return std::make_pair(false, "More than one camera index for the same image name");
                } else {
                    // Insert entry to extrinsic matrix
                    m_camIndices[imgName] = camIndex;
                }
            }
        }
        
        input.close();
    } else {
        LOG(WARNING) << "Unable to open file" << ba.constData();
        return std::make_pair(false, "Unable to open file");
    }
    
    return std::make_pair(true, "Valid extrinsic matrices and camera indices");
}
//
//std::pair<bool, std::string> ImageConvexHull::setCamIndices(const QString& path) {
////
//}

cv::Mat ImageConvexHull::convertPts3D(const std::vector<CCVector3>& pts3D) {
    cv::Mat pts3DMat(3, pts3D.size(), CV_64F);
    for (int col = 0; col < pts3D.size(); col++) {
        pts3DMat.at<double>(0, col) = pts3D.at(col).x;
        pts3DMat.at<double>(1, col) = pts3D.at(col).y;
        pts3DMat.at<double>(2, col) = pts3D.at(col).z;
    }

    return pts3DMat;
}

void ImageConvexHull::setN(const size_t n) {
    m_N = n;
}

void ImageConvexHull::setRefImagesPaths(const QStringList& imagesPaths) {
    m_refImagesPaths = imagesPaths;
}

QString ImageConvexHull::parseImgPath(const QString& imgName) {
    QFileInfo imgFileInfo(imgName);
    QString fileName = imgFileInfo.fileName();
    return fileName;
}

std::vector<std::string> split(const std::string &s, char delim) {
    std::vector<std::string> elems;
    std::stringstream ss(s);
    std::string item;
    while (std::getline(ss, item, delim)) {
        elems.push_back(item);
    }
    return elems;
}

/*
 * Only works with png, jpeg, and tiff
 */
boost::gil::point2<long> ImageConvexHull::getImageSize(const std::string& filename) {
    boost::filesystem::path extension = boost::filesystem::path(filename).extension();
    std::string ext = extension.string();
    if (boost::iequals(ext,".png")){
        return boost::gil::png_read_dimensions(filename);
    } else if (boost::iequals(ext,".jpg") || boost::iequals(ext,".jpeg")){
        return boost::gil::jpeg_read_dimensions(filename);
    } else if (boost::iequals(ext,".tiff")){
        return boost::gil::tiff_read_dimensions(filename);
    } else {
        return boost::gil::point2<long>(0,0);
    }
}
