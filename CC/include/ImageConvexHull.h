/* 
 * File:   ImageConvexHull.h
 * Author: Stefanus Candra
 * 
 * Header file for ImageConvexHull.cpp
 *
 * Created on June 21, 2014, 5:42 PM
 */

#ifndef IMAGECONVEXHULL_H
#define	IMAGECONVEXHULL_H

// OpenCV
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"

#include "CCGeom.h"

#ifdef BUILD_TEST
// GTest
#include <gtest/gtest.h>
#endif

// Qt
#include <QString>
#include <QStringList>

// Boost
#include <boost/gil/gil_all.hpp>
#include <boost/gil/extension/io/jpeg_io.hpp>
#include <boost/gil/extension/io/png_io.hpp>
#include <boost/gil/extension/io/tiff_io.hpp>
#include <boost/filesystem/path.hpp>

#include <unordered_map>

class ImageConvexHull {
    
public:

    ImageConvexHull();

    ImageConvexHull(const QStringList& imagesPaths,
            const QString& intrinsicMatPath,
            const QString& extrinsicMatPath,
            const size_t& N);

    virtual ~ImageConvexHull();

    // Placeholder to hold properties of 3D to 2D projection

    struct ProjProp3DTo2D {
        // Angle in degrees between 0-90 of where the point is on the image
        std::vector<float> angleDeg;
        // Depth of point
        cv::Mat depth;
        // Distance between origin of camera and point
        std::vector<float> dist;
        // Boolean of whether the points are in front
        cv::Mat isFront;
        // Boolean of whether the points are in image bounds
        cv::Mat isInbound;
        // 2D points after projection
        cv::Mat uv;
    };

    struct ConvexHullProp {
        cv::Mat vertices2D;
        QString imgPath;
        double convexHullArea;

        inline bool operator<(const ImageConvexHull::ConvexHullProp& a) {
            return this->convexHullArea < a.convexHullArea;
        }

        inline bool operator<=(const ImageConvexHull::ConvexHullProp& a) {
            return this->convexHullArea <= a.convexHullArea;
        }
    };

    //    void setPtrToBoundedPoints(std::vector<Vector3Tpl<float> >* boundedPoints);
    std::vector< ImageConvexHull::ConvexHullProp > getBestImages(const std::vector<CCVector3>& boundedPts);

    ImageConvexHull::ProjProp3DTo2D transAndProj3Dto2D(const std::vector<CCVector3>& pts3D, const QString& imgName, size_t height = 0, size_t width = 0);
    ImageConvexHull::ProjProp3DTo2D transAndProj3Dto2D(const cv::Mat& pts3D, const QString& imgName, size_t height = 0, size_t width = 0);

    static QString parseImgPath(const QString& imgName);
    static boost::gil::point2<long> getImageSize(const std::string& filename);

    std::pair<bool, std::string> setIntrinsicMatrices(const QString& path);
    std::pair<bool, std::string> setExtrinsicMatricesAndCamIndices(const QString& path);

    cv::Mat convertPts3D(const std::vector<CCVector3>& pts3D);
    void setRefImagesPaths(const QStringList& imagesPaths);
    void setN(const size_t n);

protected:

    QStringList m_refImagesPaths;
    // Number of images to display
    size_t m_N;
    std::unordered_map<size_t, cv::Mat> m_intrinsicMats;
    std::unordered_map<std::string, cv::Mat> m_extrinsicMats;
    std::unordered_map<std::string, size_t> m_camIndices;

    cv::Mat coordTrans3D(const cv::Mat& transMat, const cv::Mat& pts3D);

    cv::Mat getIntrinsicMat(const size_t camIndex);
    cv::Mat getExtrinsicMat(const std::string& imgName);
    const size_t getCamIndex(const std::string& imgName);

    ImageConvexHull::ProjProp3DTo2D project3DToCam2D(cv::Mat& K, cv::Mat& ptsCamt, const size_t imgHeight = 0, const size_t imgWidth = 0, cv::Mat T = cv::Mat::eye(4, 4, CV_64F));

    cv::Mat filterByCol(const cv::Mat& A, const cv::Mat& bools);    

    cv::Mat getContour(const cv::Mat& pts);

    const char* m_className;
    void initLogging();

private:

    void init(const QStringList& imagesPaths,
            const QString& intrinsicMatPath,
            const QString& extrinsicMatPath,
            const size_t N);
    
    #ifdef BUILD_TEST
    FRIEND_TEST(ImageConvexHullTest, getContourTest);
    FRIEND_TEST(ImageConvexHullTest, ConvexHullArea_SimpleTest);
    FRIEND_TEST(ImageConvexHullTest, filterByColTest);
    #endif
};

#endif	/* IMAGECONVEXHULL_H */

