/* 
 * File:   ccImgConvexHullConfigDlg.cpp
 * Author: candrastefan
 * 
 * Created on July 15, 2014, 11:55 AM
 */

#include "ccImgConvexHullConfig.h"

//qCC_db
#include <ccLog.h>
#include <iostream>

//Qt
#include <QFileDialog>
#include <QShortcut>

ccImgConvexHullConfig::ccImgConvexHullConfig(QWidget* parent)
: ccOverlayDialog(parent)
, Ui::imgConvexHullConfigDlg() {

    setupUi(this);

    // So that it doesn't emit signal when value is being changed
    //    numImg->setKeyboardTracking(false);
    //    camIndex->setKeyboardTracking(false);

    connect(intrinsicMatLoader, SIGNAL(clicked()), this, SLOT(loadIntrinsicMatPath()));
    connect(intrinsicMatLineEdit, SIGNAL(textChanged(QString)), this, SLOT(updateIntrinsicMatPath(QString)));
    connect(extrinsicMatLoader, SIGNAL(clicked()), this, SLOT(loadExtrinsicMatPath()));
    connect(extrinsicMatLineEdit, SIGNAL(textChanged(QString)), this, SLOT(updateExtrinsicMatPath(QString)));
    connect(refImgsLoader, SIGNAL(clicked()), this, SLOT(loadRefImagesPaths()));
    connect(refImagesLineEdit, SIGNAL(textChanged(QString)), this, SLOT(updateRefImagesPath(QString)));
    connect(numImg, SIGNAL(valueChanged(int)), this, SLOT(setN(int)));

    QShortcut *escape = new QShortcut(QKeySequence("Escape"), this);
    connect(escape, SIGNAL(activated()), this, SLOT(reject()));
    QShortcut *enter = new QShortcut(QKeySequence("Enter"), this);
    connect(enter, SIGNAL(activated()), this, SLOT(accept()));
}

void ccImgConvexHullConfig::loadRefImagesPaths() {
    QFileDialog fd;
    QString selfilter = tr("PNG (*.png)");
    refImagesLineEdit->setText(fd.getOpenFileNames(this,
            tr("Choose images"),
            0,
            tr("All files (*.*);;JPEG (*.jpg *.jpeg);;TIFF (*.tif);;PNG (*.png)"),
            &selfilter).join("; "));
}

ccImgConvexHullConfig::~ccImgConvexHullConfig() {
}

void ccImgConvexHullConfig::updateRefImagesPath(QString path) {
    QString simplifiedPath = path.simplified();
    QStringList list = simplifiedPath.split("; ", QString::SkipEmptyParts);
    m_refImagesPaths = list;
}

void ccImgConvexHullConfig::loadIntrinsicMatPath() {
    QFileDialog fd;
    intrinsicMatLineEdit->setText(fd.getOpenFileName(this,
            tr("Choose file containing intrinsic transformation matrices"),
            0,
            tr("All (*.*)")));
}

void ccImgConvexHullConfig::loadExtrinsicMatPath() {
    QFileDialog fd;
    extrinsicMatLineEdit->setText(fd.getOpenFileName(this,
            tr("Choose file containing extrinsic transformation matrices"),
            0,
            tr("All (*.*)")));
}

void ccImgConvexHullConfig::updateIntrinsicMatPath(QString path) {
    m_intrinsicMatPath = path;
}

void ccImgConvexHullConfig::updateExtrinsicMatPath(QString path) {
    m_extrinsicMatPath = path;
}

QString ccImgConvexHullConfig::getIntrinsicMatPath() {
    return m_intrinsicMatPath;
}

QString ccImgConvexHullConfig::getExtrinsicMatPath() {
    return m_extrinsicMatPath;
}

QStringList ccImgConvexHullConfig::getRefImagesPaths() {
    return m_refImagesPaths;
}

size_t ccImgConvexHullConfig::getN() {
    return m_N;
}

void ccImgConvexHullConfig::setN(const int n) {
    m_N = n;
}
